import 'dotenv/config'
import express from 'express'
import fileUpload from 'express-fileupload'
import axios from 'axios'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

const { APP_NAME, PROSTORE_HOST, PROSTORE_PORT } = process.env

const app = express()
app.use(fileUpload({ createParentPath: true }))

/**
 * Gritting request
 */
app.get('/api', (req, res) => {
    const current_time = dayjs.utc().format()

    const name = req.query.name?.trim() || APP_NAME || 'World'

    return res.send({
        message: `Hello, ${name}!`,
        at: current_time,
    })
})

/**
 * Upload files
 */
app.post('/api/upload', (req, res) => {
    if (!req.files) {
        return res.status(422)
            .send({ message: 'No files uploaded!' })
    }

    const files = req.files

    const names = Object.keys(files)

    const rows = names.map(elem => ({
        name: files[elem].name,
        size: files[elem].size,
        md5: files[elem].md5,
    }))

    return res.send({
        count: names.length,
        info: rows,
    })
})

/**
 * Get data from datamart schema
 */
app.post(`/api/:datamart/:table`, async (req, res) => {
    const { datamart, table } = req.params

    const limit = req.params.limit || 10
    const offset = req.params.offset || 0

    try {
        const { data } = await axios.post(
            `http://${PROSTORE_HOST}:${PROSTORE_PORT}/query/execute`, {
                // sql: 'select 1',
                sql: `select * from ${datamart}.${table} limit ${limit} offset ${offset}`,
            }, {
                headers: { 'Content-Type': 'application/json' },
            })

        return res.send(data)
    } catch (error) {
        return res.status(422)
            .send({
                message: error.response?.data?.exceptionMessage || error.message,
            })
    }
})

app.listen(3030)
