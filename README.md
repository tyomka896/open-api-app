# Сервис работы с OpenAPI Spec для Агента СМЭВ4

Это небольшой локальный сервис, предназначенный для простого взаимодействия и проверки спецификации Open API. Данный сервис позволяет взаимодействовать с Prostore, если у вас развернута Витрина Лайт или Стандарт.

## Установка

Перед использованием проверить наличие пакетов и установить, если отсутствуют:

```bash
git --version
docker --version
docker compose --version
```

Скачать репозиторий в отдельную директорию:

```bash
git clone https://gitlab.com/tyomka896/open-api-app.git open-api
```

Перейти в директорию проекта:

```bash
cd open-api
```

Создать файл с переменными на основе шаблона:

```bash
cp .env.example .env
```

Открыть файл `.env` любым редактором, подставить собственные значения и сохранить. В файле описаны следующие параметры:

- *APP_NAME* - наименование приложения, которое будет возвращаться в ответе на запрос
- *API_PORT* - порт работы API Gateway сервиса, указанного в конфигурации Агента, на который будут поступать запросы
- *BASE_PATH* - наименование OpenAPI спецификации, зарегистрированного в ЛКУВ
- *PROSTORE_HOST* - адрес Prostore для проверки взаимодействия с Витриной
- *PROSTORE_PORT* - порт Prostore

Для запуска сервиса из директории `open-api` выполнить:

```bash
docker compose up -d --force-recreate
```

> После любых изменений конфигураций можно перезапустить этой же командой

Успешность запуска сервиса можно определить сообщениями:

```
Creating api-gateaway-app  ... done
Creating api-nginx-app     ... done
```

Для полного удаления развернутых сервисов:

```bash
docker compose down --rmi all
```

## Использование

После успешного запуска сервиса будет доступно три запроса по адресу `localhost:12321`:

- __GET__ */api* - тестовое обращение к сервису

  Пример:

    ```bash
    curl -X GET localhost:12321/api
    # С параметром 'name'
    curl -X GET localhost:12321/api?name=MyApp
    # С указанием имени OpenAPI спецификации
    curl -X GET localhost:12321/my-base-path/api?name=Spec
    ```

  Ответ:

  ```json
  {
      "message": "Hello, OpenAPI!",
      "at": "YYYY-MM-DDThh:mm:ssZ"
  }
  ```

- __POST__ */api/upload* - загрузка файлов

  Пример:

  ```bash
  curl -X POST -F filename=@{file_name} localhost:12321/api/upload
  ```

  Ответ:

  ```json
  {
    "count": 1,
    "info": [
        {
            "name": "string",
            "size": 27182,
            "md5": "13e9d7677ba93d762a66eee9366bff13"
        }
    ]
  }
  ```

- __POST__ */api/{datamart}/{table}* - обращение к `Prostore` по мнемоникам витрины и таблицы. Принимает два опциональных параметра:
  - *limit* - ограничить количество строк (по умолчанию 10)
  - *offset* - пропустить количество строк (по умолчанию 0)

  Пример:

  ```bash
  curl -X POST localhost:12321/api/datamart/table
  # С параметрами 'limit' и 'offset'
  curl -X POST 'localhost:12321/api/datamart/table?limit=15&offset=5'
  ```

  Ответ:

  ```json
  {
    "requestId": "925d9356−68f8−4ee2−89a9−30fde0474bfb",
    "result": [
        {},
    ],
    "metadata": [
        {},
    ]
  }
  ```

  SQL-запрос к Prostore составляется  в формате:

  ```sql
  SELECT * FROM {datamart}.{table} limit {limit} offset {offset};
  ```
